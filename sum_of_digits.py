# Given n, take the sum of the digits of n. 
# If that value has more than one digit, continue reducing in this way until a single-digit number is produced. 
# The input will be a non-negative integer.

#     16  -->  1 + 6 = 7
#    942  -->  9 + 4 + 2 = 15  -->  1 + 5 = 6
# 132189  -->  1 + 3 + 2 + 1 + 8 + 9 = 24  -->  2 + 4 = 6
# 493193  -->  4 + 9 + 3 + 1 + 9 + 3 = 29  -->  2 + 9 = 11  -->  1 + 1 = 2




# TODO:
# if the user input is less than 10, show the message
# convert the user input (n) to string
# create a variable named x, and put n
# create a variable named r, and put 0
# ---> while len(x) > 1: 
#   ---> for i in range(len(x)):
#   put r + int(x[i]) to the variable r
#   <--- for
# convert r to string and put it into x
# <--- while
# return x

def sum_of_digits(n):

    if int(n) < 10:
        print("The input should be more than 10")
        return n

    # x = str(n)
    while len(x) > 1:
        r = sum_number_of_digit(x)
        x = str(r)
    return x

def sum_number_of_digit(x:str):
    r = 0
    for i in range(len(x)):
        r = r + int(x[i])
    return r




if __name__ == '__main__':
    val = str(input("input: "))
    result = sum_of_digits(val)
    print(result)


