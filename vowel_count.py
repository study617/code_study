# Return the number (count) of vowels in the given string.
# We will consider a, e, i, o, u as vowels for this Kata (but not y).
# The input string will only consist of lower case letters and/or spaces.


#TODO: 
# create a list named vowle_list
# put "a","e","i","o","u" into the vowle_list
# create a variable named count
# put 0 into the count
# --> loop
# if vowels are included in the input, add 1 to the count
# <-- loop
# return count


def vowles(val):
    vowle_list = ["a","e","i","o","u"]
    count = 0

    for i in val:
        if i in vowle_list:
            count += 1
    
    return count


if __name__ == '__main__':
    val = str(input("input: "))
    result = vowles(val)
    print(result)
