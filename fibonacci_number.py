
# 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711
# F0 = 0,
# F1 = 1,
# Fn = Fn-1 + Fn-2 (n ≥ 0)


# TODO:
# get the input from user
# create variable named a and b
# if the input is 0 or 1, return 0 or 1
# if input is neither of the above, n-1 + n-2
# --> loop(n)
# c = a + b
# a = b
# b = c
# print b
# <-- loop
# return b

def fibonacci_1(n):
    a, b = 1, 0

    if n == 0 or n == 1:
        return n

    for _ in range(n):
        # if n is 3:
        # loop_1
            # c = 1 + 0
            # a = 0
            # b = 1
        # loop_2
            # c = 0 + 1
            # a = 1
            # b = 1
        # loop_3
            # c = 1 + 1
            # a = 1
            # b = 2

        a, b = b, a + b

        # print(b)

    return b


# TODO:
# get the input from user
# if the input is 0 or 1, return [0] or [1]
# if input is neither of the above, n-1 + n-2
# create a list named sequence, and put 0 and 1 (sequence = [0, 1])
# --> while len(sequence) <= n
# sequence[len(sequence) - 1] + sequence[len(sequence) - 2]
# insert the result to variable named next_value
# append next_value to sequence
# <-- while
# return sequence[-1]


def fibonacci_2(n):
    if n == 0 or n == 1:
        return [n]

    sequence = [0, 1]
    while len(sequence) <= n:
        next_value = sequence[len(sequence) - 1] + sequence[len(sequence) - 2]
        sequence.append(next_value)

    return sequence[-1]




# recursive function
def fibonacci_3(n):
    if n == 0 or n == 1:
        return n
    else:
        return fibonacci_1(n - 1) + fibonacci_1(n - 2)



if __name__ == '__main__':
    input = int(input("input: "))
    result = fibonacci_2(input)
    print(result)


