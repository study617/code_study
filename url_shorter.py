
# Task
# URL Shortener
# Write a method shorten, which receives a long URL and returns a short URL starting with short.ly/, consisting only of lowercase letters (and one dot and one slash) and max length of 13.

# Note: short.ly/ is not a valid short URL.

# Redirect URL
# Write a method redirect, which receives the shortened URL and returns the corresponding long URL.

import random
import string


def shorten():
    long_url = 'https://www.google.com/search?q=codewars&tbm=isch&ved=2ahUKEwjGuLOJjvjsAhXNkKQKHdYdDhUQ2-cCegQIABAA&oq=codewars&gs_lcp=CgNpbWcQAzICCAAyBAgAEB4yBAgAEB4yBAgAEB4yBAgAEB4yBAgAEB4yBAgAEB4yBAgAEB4yBAgAEB4yBggAEAUQHlDADlibD2CjEGgAcAB4AIABXIgBuAGSAQEymAEAoAEBqgELZ3dzLXdpei1pbWfAAQE&sclient=img&ei=RJmqX8aGHM2hkgXWu7ioAQ&bih=1099&biw=1920#imgrc=Cq0ZYnAGP79ddM'
    short_url = ''.join(random.choice(string.ascii_lowercase + string.digits) for x in range(13))

    short_url = 'https://bit.ly/' + short_url

    url_dict = {short_url: long_url}

    return url_dict


    print(short_url)

def redirect():
    url_dict = shorten()

    return url_dict['short_url']




if __name__ == '__main__':
    url = shorten()
    print(url)




