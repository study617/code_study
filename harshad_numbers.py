def solution_1(n):
    transform = list(str(n))

    sum = 0

    # Add the input number　==> 19
    for x in transform:
        sum += int(x)
    sum_reversed = str(sum)

    # Try to reverse ==> 91
    sum_reversed = sum_reversed[::-1] 

    # 19 * 91
    total_sum = sum*int(sum_reversed)

    if total_sum == n: 

        return True
    else:
        return False   



def solution_2(num):

    transform = list(str(num))
    
    x = 0
    sum1 = 0
    sum2 = 0
        
    #Make a copy of num and store it in variable n    
    n = num
        
    #Calculates sum of digits    
    while(num > 0):    
        x = num%10
        sum1 = sum1 + x 
        num = num//10
        
    #Checks whether the number is divisible by the sum of digits    
    if(n%sum1 == 0):    
        print(str(n) + " is a harshad number")

        # Add the input number　==> 19
        for x in transform:
            sum2 += int(x)
        sum_reversed = str(sum2)

        # Try to reverse ==> 91
        sum_reversed = sum_reversed[::-1] 

        # 19 * 91
        total_sum = sum2*int(sum_reversed)

        if total_sum == n: 

            return True
        else:
            return False   

    else:    
        print(str(n) + " is not a harshad number")



if __name__ == '__main__':
    r = solution_2(1729)
    # solution_2(1234)
